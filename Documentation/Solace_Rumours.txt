
2  - Baseline
10 - general knowledge
20 - less knowable
30 - very uncommon
40 - almost unknowable

DC: 14
I heard the training golems that the mages use were students that didn't return their library books.

Valurian
DC: 20
I've heard the rumours about the origin of our training golems. Quite amusing, aren't they?

Tailor
DC: 10
If you want to recolour an outfit, you should get some dyes from nini.
DC: 15
I get nini to help me with my dyes. There aren't a lot of pigments around these parts, but she makes do with a little alchemy.

Nini
DC: 13
No one's really sure where Art gets those crystals from.

Madie
DC:

DC: 10
If you're desperate, the best way to make a living these days is to go out hunting. Supplies are limited, and some people can get rich bringing back whatever they can find.

DC: 10
You'll sleep a lot better if you stay at an inn than if you were to go out camping.

DC: 10
If you're going to camp out in the wilderness, be sure to buy a bedroll from Gruud's Goods. Having a good layer between you and the ground helps keep you warm.

DC: 14
If you see a ledge with spikes on it, stand next to it and use a climbing rope on yourself. You can get up and down that way, and you can use it on your friends to pull them over to you.

DC: 8
I'll bet people who are more persuasive hear more interesting things.

DC: 9
It's best to avoid going outside.

DC: 8
There's no shame in staying at the Downtrod Inn. We can't all afford nice things.

DC: 10
Don't underestimate goblins. They are far stronger than you know.

DC: 10
If you die, you stay dead. That's no joke.

DC: 10
It's never worth risking your life doing something foolish.

DC: 20
Sometimes it can be worth it risking your life, even if it might seem foolish.

DC: 12
Some of the creatures out there carry nasty diseases. If you get sick, you can find potions of lesser restoration at Fizzletop's

DC: 14
Sometimes it is impossible to see an ambush coming, even for those with the keenest senses.



DC: 15
Emerson NPC's only
It hasn't been safe outside for generations, but there will always be solace in Emerson.

DC: 17
A lot of folk passing through here aren't sure where they came from. Most of them don't last very long.

DC: 12
They say there's a lot more rain on the other side of the mountain.

DC: 20
Did you know there's a monastary on the mountain above Emerson?

DC: 16
Sycla 
This tree is far older than anything else born in Emerson. It was planted many years ago by my grandmother.

DC: 21
Casio was once home to a good number of folk living in Emerson.

DC: 8
Samir
That Nania knows a few tricks... if you know what I mean.

DC: 8
Nania
Samir says I know a few tricks, but he's just blowing hot steam.

DC: 14
Some of the goblins out there carry poisonous bullets. Merchants pay pretty well for them.

DC: 20
There are mages who charm the people they trade with. Some call it dishonest, but it sure makes the prices better.

DC: 20
I don't trust that talking penguin in the Battle Chest. I think he might be from another plane.

DC: 18
The guild is in charge of pretty much everything in Emerson now, but it didn't always use to be that way.

DC: 20
Before the guild, Emerson was ruled by a lord...........................................................................

DC: 8
You'd think there'd be a guild leader for rogues, wouldn't you? I've never seen her around.

DC: 14

DC: 20
You'd have to really search for it, but there's a secret door hidden amongst the bookshelves in the west hall of the guild.

DC: 23

DC: 25
It's actually not that hard to tell that Ster isn't like the rest of us. He's got dragon's blood coursing through his veins.

Baliana----------------------------------------------------------------
Bards, Paladins, Blackguards, and Sorcerors Only
DC: 10
Sometimes, it's our job to be as charming and persuasive as possible so we can learn as much as we can. Many grand quests were based in the pursuit of a rumor, sometimes just to see if it were true.
DC: 10
Shape up, square your shoulders, and speak from the diaphragm! 
DC: 12
There are many who say that charisma is a matter of attractiveness, but that is only a factor. Charisma is a measure of your personal strength. Gusto! Confidence! People and creatures alike are very attracted by confidence. It's something you should use to your advantage.
DC: 15


Bards only
DC: 13
Nothing turns a fight around quite like singing a curse song as part of your battle song.
DC: 15
The song is the bard's greatest power. At it's full potential, you can even bolster the basic skills of your allies, helping your rogue break into locks and traps and such.

Amothiel----------------------------------------------------------------
DC: 15
I really wish Baliana would stay in the guild hall and teach things here like a proper guild leader should! ... Oh dear, please don't tell her I said that.



Glendir
DC: 2
Why don't you come and have a seat, friend? There's nothing like the sound of running water to cleanse the spirit.
DC: 10
It feels cool and fresh here because this water comes down from the mountainside. Did you know it's always snowing up there?
DC: 12

DC: 14
DC: 16
DC: 18
DC: 20



